package templates

import (
	"fmt"
	"net/http"
	"time"
)

type myMap map[string]interface{}

// Page is a struct allowing data to be passed to the template
type Page struct {
	Data   myMap
	Locale int
	Url    string
	alts   []*Alternative
	w      http.ResponseWriter
	Start  time.Time
}

// MakeMap allows the initialization of a myMap-instance, { key1, value1, key2, value2, ... }
func MakeMap(keyValue ...interface{}) myMap {
	var key string
	miniMap := make(map[string]interface{})
	if len(keyValue) == 1 {
		if kv, ok := keyValue[0].([]interface{}); ok {
			keyValue = kv
		}
	}
	for i, v := range keyValue {
		if i%2 == 0 {
			if str, ok := v.(string); ok {
				key = str
			} else {
			}
		} else {
			miniMap[key] = v
			key = ""
		}
	}
	return miniMap
}

func (p *Page) Get(key string) interface{} {
	return p.Data.Get(key)
}

func (m *myMap) Get(key string) interface{} {
	return (*m)[key]
}

func (p *Page) GetUrl(key int) string {
	return GetUrlByLocale(p.Locale, key)
}

func NewPage(keyValue ...interface{}) *Page {
	p := &Page{}
	p.Data = MakeMap(keyValue)
	return p
}

func (p *Page) IsSet(something interface{}) bool {
	return fmt.Sprintf("%v", something) != "<nil>"
}

// Flush allows the page to flush whatever can be flushed if SPDY is not used
func (p *Page) Flush() string {
	if f, ok := p.w.(http.Flusher); ok {
		f.Flush()
	}
	return ""
}

func (p *Page) Lang(s string) string {
	if val, ok := (*config.Language)[p.Locale][s]; ok {
		return val
	} else {
		return "No translation for " + s
	}
}

func (p *Page) Plus(a, b int) int {
	return a + b
}

func (p *Page) Times(a, b float64) float64 {
	return a * b
}

func (p *Page) DivideInt64(a, b int64) int64 {
	return a / b
}

func (p *Page) BenchNanoseconds() int64 {
	return time.Now().Sub(p.Start).Nanoseconds()
}
