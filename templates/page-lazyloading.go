package templates

type LazyInterface interface {
	String(loc int) string
}

func (p *Page) Lazy(lazy LazyInterface) string {
	return lazy.String(p.Locale)
}
