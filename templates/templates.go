package templates

import (
	"bytes"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/HuperWebs/go-html-combiner/combiner"
	"github.com/julienschmidt/httprouter"
	"github.com/kataras/iris"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/html"
)

type Config struct {
	Locales       *map[string]int
	URLs          *map[int]map[int]string
	Language      *map[int]map[string]string
	ProjectView   string
	Handler       PageHandler
	DefaultLocale string
	Minify        bool
}

// Init makes sure all variables are set up correctly before any function can be used
func Init(c *Config) {
	if c == nil {
		panic("Invalid configuration")
	}
	config = c
}

var config *Config

func Render(c *iris.Context, tmpl string, p *Page) {
	RenderWithStatus(c, tmpl, p, http.StatusOK)
}

func RenderWithStatus(c *iris.Context, templateName string, p *Page, status int) {
	// Disable cache - TODO: why?
	if len(c.RequestHeader("Cache-Control")) == 0 {
		c.SetHeader("Cache-Control", "no-cache")
	}

	// Be sure we're not writing stuff to an empty page
	if p == nil {
		p = NewPage()
	}

	if config.Handler != nil {
		log.Println("Warning: config.Handler not yet supported")
	}

	c.SetHeader("Content-Type", "text/html")
	c.SetStatusCode(status)

	// TODO: can we do this without mutex?
	publicTemplatesMu.RLock()
	tmpl, ok := publicTemplates[filepath.Join(config.ProjectView, templateName)]
	publicTemplatesMu.RUnlock()

	// TODO: return error?
	if !ok {
		log.Println("Could not find template:", filepath.Join(config.ProjectView, templateName))
		return
	}

	var buffer bytes.Buffer
	if config.Minify {
		err := tmpl.Execute(&buffer, p)
		if err != nil {
			log.Println(err)
			// TODO: InternalServerError for iris?
			//InternalServerError(c, c, err)
			return
		}
		// TODO: is c.Response.BodyWriter() safe?
		err = m.Minify("text/html", c.Response.BodyWriter(), &buffer)
		if err != nil {
			log.Println(err)
		}
	} else {
		err := tmpl.Execute(c.Response.BodyWriter(), p)
		if err != nil {
			log.Println(err)
		}
	}
}

// ExecuteTemplate executes the given template using the Page given, setting Cache-Control if unset
func Execute(w http.ResponseWriter, r *http.Request, templateName string, p *Page) {
	ExecuteTemplateStatus(w, r, templateName, p, http.StatusOK)
}

func ExecuteTemplateStatus(w http.ResponseWriter, r *http.Request, templateName string, p *Page, status int) {
	if orig := w.Header().Get("Cache-Control"); len(orig) == 0 {
		w.Header().Set("Cache-Control", "no-cache")
	}
	if p == nil {
		p = NewPage()
	}

	p.Url = r.RequestURI
	p.w = w
	p.Locale = GetLocale(r)

	if config.Handler != nil {
		config.Handler.Handle(w, r, p)
	}

	if orig := w.Header().Get("Content-Type"); len(orig) == 0 {
		w.Header().Set("Content-Type", "text/html")
	}
	p.Start = time.Now()

	w.WriteHeader(status)

	publicTemplatesMu.RLock()
	tmpl, ok := publicTemplates[filepath.Join(config.ProjectView, templateName)]
	publicTemplatesMu.RUnlock()

	if !ok {
		log.Println("Could not find template:", filepath.Join(config.ProjectView, templateName))
		return
	}

	var buffer bytes.Buffer
	if config.Minify {
		err := tmpl.Execute(&buffer, p)
		if err != nil {
			log.Println(err)
			InternalServerError(w, r, err)
			return
		}
		err = m.Minify("text/html", w, &buffer)
		if err != nil {
			log.Println(err)
		}
	} else {
		err := tmpl.Execute(w, p)
		if err != nil {
			log.Println(err)
		}
	}
}

// ### Preloading templates ###

var publicTemplates = make(map[string]*template.Template)
var publicTemplatesMu sync.RWMutex
var templates []string

// PreloadTemplate adds the given template names to the queue to be preloaded
func PreloadTemplate(templateName ...string) {
	for _, tn := range templateName {
		templates = append(templates, tn)
	}
}

// PreloadTemplates actually preloads the queued template names
func PreloadTemplates() {
	for _, filename := range templates {
		preloadTemplate(filename)
	}
}

func preloadTemplate(filename string) {
	// Read file
	d, err := os.Getwd()
	if err != nil {
		LError.Println(err)
		return
	}

	err = os.Chdir(config.ProjectView)
	if err != nil {
		LError.Println(err)
		return
	}
	defer os.Chdir(d)

	f, err := os.Open(filename)
	if err != nil {
		LError.Println(err)
		return
	}

	preloadTemplateReader(filename, f)
}

func preloadTemplateReader(n string, f io.Reader) {
	c, err := combiner.CombineReader("", f)
	if err != nil {
		LError.Println(err)
		return
	}
	s := string(c)

	// Save it to the list
	var name string
	if filepath.IsAbs(n) {
		name = n
	} else {
		name = filepath.Join(config.ProjectView, n)
	}

	var tmpl *template.Template
	tmpl = template.New(name)
	publicTemplatesMu.Lock()
	publicTemplates[name] = tmpl
	publicTemplatesMu.Unlock()

	_, err = tmpl.Parse(s)
	if err != nil {
		LError.Println(err)
		return
	}
	tmpl.ParseFiles()
}

var HttpMux = httprouter.New()

var m *minify.M

func init() {
	m = minify.New()
	m.AddFunc("text/html", html.Minify)
}
