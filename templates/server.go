package templates

import (
	"net/http"

	"fmt"
	"github.com/julienschmidt/httprouter"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func InternalServerError(w http.ResponseWriter, r *http.Request, e error) {
	LError.Output(2, fmt.Sprintln(e))
	if len(ErrorPage) == 0 || !strings.Contains(r.Header.Get("accept"), "text/html") {
		WritePlain(w, http.StatusInternalServerError, "Status 500 - Internal Server Error\r\n"+e.Error())
		return
	}
	ExecuteTemplateStatus(w, r, ErrorPage, NewPage(
		"code", http.StatusInternalServerError,
		"title", "Internal Server Error",
		"error", template.HTML("The following error has been reported: <strong>"+e.Error()+"</strong>. "),
	), http.StatusInternalServerError)
}

func NotImplemented(w http.ResponseWriter, r *http.Request) {
	LWarning.Output(2, fmt.Sprintln("not implemented"))
	if len(ErrorPage) == 0 || !strings.Contains(r.Header.Get("accept"), "text/html") {
		WritePlain(w, http.StatusNotImplemented, "Status 501 - Action not implemented\r\nWe do not supprt whatever it is you're trying to do. ")
		return
	}
	ExecuteTemplateStatus(w, r, ErrorPage, NewPage(
		"code", http.StatusNotImplemented,
		"title", "Not Implemented",
		"error", "The thing you are trying to do, hasn't been programmed yet. ",
	), http.StatusNotImplemented)
}

func Forbidden(w http.ResponseWriter, r *http.Request) {
	LWarning.Output(2, fmt.Sprintln(r.RemoteAddr, "is forbidden"))
	if len(ErrorPage) == 0 || !strings.Contains(r.Header.Get("accept"), "text/html") {
		WritePlain(w, http.StatusForbidden, "Status 403 - Forbidden\r\nWe know who you are, but you are not authorized. ")
		return
	}
	ExecuteTemplateStatus(w, r, ErrorPage, NewPage(
		"code", http.StatusForbidden,
		"title", "Forbidden",
		"error", "We know who you are, but you are not authorized. ",
	), http.StatusForbidden)
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	LInfo.Output(2, fmt.Sprintln(r.RemoteAddr, "couldn't find", r.URL.Path))
	if len(ErrorPage) == 0 || !strings.Contains(r.Header.Get("accept"), "text/html") {
		WritePlain(w, http.StatusNotFound, "Status 404 - Not Found\r\nThe page or resource you are trying to access, does not exist. ")
		return
	}
	ExecuteTemplateStatus(w, r, ErrorPage, NewPage(
		"code", http.StatusNotFound,
		"title", "Page Not Found",
		"error", "The page or resource you are trying to access, does not exist. ",
	), http.StatusNotFound)
}

func BadRequest(w http.ResponseWriter, r *http.Request) {
	if len(ErrorPage) == 0 || !strings.Contains(r.Header.Get("accept"), "text/html") {
		WritePlain(w, http.StatusBadRequest, "Status 400 - Bad Request\r\nThere was something wrong with the request, but we do not know what. ")
		return
	}
	ExecuteTemplateStatus(w, r, ErrorPage, NewPage(
		"code", http.StatusBadRequest,
		"title", "Bad Request",
		"error", "There was something wrong with the request, but we do not know what. ",
	), http.StatusBadRequest)
}

func POST(url string, handler httprouter.Handle) {
	HttpMux.POST(url, handler)
}

func GET(url string, handler httprouter.Handle) {
	HttpMux.GET(url, handler)
}

var (
	LTrace   = log.New(ioutil.Discard, "TRACE:\t\t", log.Ldate|log.Ltime|log.Lshortfile)
	LInfo    = log.New(os.Stdout, "INFO:\t\t", log.Ldate|log.Ltime|log.Lshortfile)
	LWarning = log.New(os.Stdout, "WARNING:\t", log.Ldate|log.Ltime|log.Lshortfile)
	LError   = log.New(os.Stderr, "ERROR:\t\t", log.Ldate|log.Ltime|log.Lshortfile)

	ErrorPage string
)
