package templates

type myTupleMap map[int]interface{}

type Tuple struct {
	data myTupleMap
}

func (t *Tuple) Get(number int) interface{} {
	return t.data[number]
}

func (p *Page) Tuple(somethings ...interface{}) *Tuple {
	t := &Tuple{}
	t.data = make(map[int]interface{})
	for i, thing := range somethings {
		t.data[i] = thing
	}
	return t
}
