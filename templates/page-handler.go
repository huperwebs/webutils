package templates

import "net/http"

type PageHandler interface {
	Handle(http.ResponseWriter, *http.Request, *Page)
}

type dummyHandler struct {
	// contain nothing
}

func (dummy dummyHandler) Handle(http.ResponseWriter, *http.Request, *Page) {
	// do nothing
}

func DefaultHandler() PageHandler {
	return dummyHandler{}
}
