package templates

import (
	"bytes"
	"database/sql"
	"html/template"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/boltdb/bolt"
	"github.com/hoisie/mustache"
	tidblog "github.com/ngaut/log"
	"github.com/pingcap/tidb"
	_ "github.com/pingcap/tidb"
)

const benchmarkTemplate = `
<!DOCTYPE html>
<html>
<head><title>Hi!</title></head>
<body><h1>Hi there!</h1></body>
</html>
`
const benchmarkTemplateName = "benchmark"

var boltdb *bolt.DB
var tidbdb *sql.DB
var getTemplateStmt *sql.Stmt

func init() {
	Init(&Config{
		ProjectView: "./",
	})

	var err error
	boltdb, err = bolt.Open("/tmp/boltdb", os.FileMode(0777), nil)
	if err != nil {
		log.Fatal(err)
		return
	}

	boltdb.Update(func(tx *bolt.Tx) error {
		bucky, err := tx.CreateBucketIfNotExists([]byte("templates"))
		if err != nil {
			return err
		}
		return bucky.Put([]byte(benchmarkTemplateName), []byte(benchmarkTemplate))
	})

	dbPath := "/tmp/tidb"
	dbName := "tidb"
	tidb.Debug = false
	tidblog.SetLevel(tidblog.LOG_LEVEL_NONE)

	tidbdb, err = sql.Open("tidb", "goleveldb://"+dbPath+"/"+dbName)
	if err != nil {
		log.Fatal("Cannot open", err)
	}

	_, err = tidbdb.Exec("CREATE TABLE IF NOT EXISTS templates (name char(100) PRIMARY KEY, data TEXT)")
	if err != nil {
		log.Fatal("Cannot create", err)
	}

	_, err = tidbdb.Exec("INSERT INTO templates (name, data) VALUES (?, ?) ON DUPLICATE KEY UPDATE data=data",
		benchmarkTemplateName, benchmarkTemplate)
	if err != nil {
		log.Fatal("Cannot insert", err)
	}

	getTemplateStmt, err = tidbdb.Prepare("SELECT data FROM templates WHERE name = ?")
	if err != nil {
		log.Fatal("Cannot prepare", err)
	}
}

func BenchmarkTemplateExecuteBuiltin(bench *testing.B) {
	b := bytes.NewBufferString(benchmarkTemplate)
	preloadTemplateReader(benchmarkTemplateName, b)

	var w httptest.ResponseRecorder
	var r http.Request

	bench.ResetTimer()

	for i := 0; i < bench.N; i++ {
		Execute(&w, &r, benchmarkTemplateName, nil)
	}
}

func BenchmarkTemplateExecuteMustache(bench *testing.B) {
	templ, err := mustache.ParseString(benchmarkTemplate)
	if err != nil {
		bench.Fail()
		return
	}

	bench.ResetTimer()

	for i := 0; i < bench.N; i++ {
		templ.Render()
	}
}

func BenchmarkTemplateBuiltin(bench *testing.B) {
	b := bytes.NewBufferString(benchmarkTemplate)

	for i := 0; i < bench.N; i++ {
		var tmpl *template.Template
		tmpl = template.New(benchmarkTemplateName)
		_, err := tmpl.Parse(b.String())
		if err != nil {
			bench.Fail()
			return
		}

		var w = &httptest.ResponseRecorder{}
		var r = &http.Request{}
		var p *Page

		if orig := w.Header().Get("Cache-Control"); len(orig) == 0 {
			w.Header().Set("Cache-Control", "no-cache")
		}
		if p == nil {
			p = NewPage()
		}

		p.Url = r.RequestURI
		p.w = w
		p.Locale = GetLocale(r)

		if config.Handler != nil {
			config.Handler.Handle(w, r, p)
		}

		if orig := w.Header().Get("Content-Type"); len(orig) == 0 {
			w.Header().Set("Content-Type", "text/html")
		}

		w.WriteHeader(http.StatusOK)

		err = tmpl.Execute(w, p)
		if err != nil {
			bench.Fail()
			return
		}
	}
}

func BenchmarkTemplateMustache(bench *testing.B) {
	for i := 0; i < bench.N; i++ {
		templ, err := mustache.ParseString(benchmarkTemplate)
		if err != nil {
			bench.Fail()
			return
		}

		var w = &httptest.ResponseRecorder{}
		var r = &http.Request{}
		var p *Page

		if orig := w.Header().Get("Cache-Control"); len(orig) == 0 {
			w.Header().Set("Cache-Control", "no-cache")
		}
		if p == nil {
			p = NewPage()
		}

		p.Url = r.RequestURI
		p.w = w
		p.Locale = GetLocale(r)

		if config.Handler != nil {
			config.Handler.Handle(w, r, p)
		}

		if orig := w.Header().Get("Content-Type"); len(orig) == 0 {
			w.Header().Set("Content-Type", "text/html")
		}

		w.WriteHeader(http.StatusOK)
		w.WriteString(templ.Render(p))
	}
}

func BenchmarkTemplateBoltDBBuiltin(bench *testing.B) {
	var templateString string
	boltdb.View(func(tx *bolt.Tx) error {
		bucky := tx.Bucket([]byte("templates"))
		data := bucky.Get([]byte(benchmarkTemplateName))
		templateString = string(data)
		return nil
	})

	for i := 0; i < bench.N; i++ {
		var tmpl *template.Template
		tmpl = template.New(benchmarkTemplateName)
		_, err := tmpl.Parse(templateString)
		if err != nil {
			bench.Fail()
			return
		}

		var w = &httptest.ResponseRecorder{}
		var r = &http.Request{}
		var p *Page

		if orig := w.Header().Get("Cache-Control"); len(orig) == 0 {
			w.Header().Set("Cache-Control", "no-cache")
		}
		if p == nil {
			p = NewPage()
		}

		p.Url = r.RequestURI
		p.w = w
		p.Locale = GetLocale(r)

		if config.Handler != nil {
			config.Handler.Handle(w, r, p)
		}

		if orig := w.Header().Get("Content-Type"); len(orig) == 0 {
			w.Header().Set("Content-Type", "text/html")
		}

		w.WriteHeader(http.StatusOK)

		err = tmpl.Execute(w, p)
		if err != nil {
			bench.Fail()
			return
		}
	}
}

func BenchmarkTemplateTidbBuiltin(bench *testing.B) {
	var templateString string
	rows, err := getTemplateStmt.Query(benchmarkTemplateName)
	if err != nil {
		bench.Fail()
		return
	}
	for rows.Next() {
		err = rows.Scan(&templateString)
		if err != nil {
			bench.Fail()
			return
		}
	}

	for i := 0; i < bench.N; i++ {
		var tmpl *template.Template
		tmpl = template.New(benchmarkTemplateName)
		_, err := tmpl.Parse(templateString)
		if err != nil {
			bench.Fail()
			return
		}

		var w = &httptest.ResponseRecorder{}
		var r = &http.Request{}
		var p *Page

		if orig := w.Header().Get("Cache-Control"); len(orig) == 0 {
			w.Header().Set("Cache-Control", "no-cache")
		}
		if p == nil {
			p = NewPage()
		}

		p.Url = r.RequestURI
		p.w = w
		p.Locale = GetLocale(r)

		if config.Handler != nil {
			config.Handler.Handle(w, r, p)
		}

		if orig := w.Header().Get("Content-Type"); len(orig) == 0 {
			w.Header().Set("Content-Type", "text/html")
		}

		w.WriteHeader(http.StatusOK)

		err = tmpl.Execute(w, p)
		if err != nil {
			bench.Fail()
			return
		}
	}
}
