package templates

import (
	"github.com/gorilla/context"
	"net/http"
	"path/filepath"
	"strings"
)

// GetLocale accesses the locale-integer set by LanguageHandler, and returning it - panics on failure.
func GetLocale(r *http.Request) int {
	locInterface := context.Get(r, "locale")
	if locInt, ok := locInterface.(int); ok && locInt > 0 {
		return locInt
	} else {
		return 0
	}
}

// GetUrl returns the localised-url for the given request
func GetUrl(r *http.Request, index int) string {
	return GetUrlByLocale(GetLocale(r), index)
}

// GetUrlByLocale returns the localised-url for the given locale and url
func GetUrlByLocale(locale int, index int) string {
	return "/" + filepath.Join((*config.Language)[locale]["locale"], strings.ToLower((*config.URLs)[locale][index]))
}
