package templates

type Language struct {
	Nr    string
	Index int
	Name  string
}

func (p *Page) Languages() []Language {
	langs := make([]Language, len(*config.Locales))
	counter := 0
	for loc, key := range *config.Locales {
		language_string := ""
		if val, ok := (*config.Language)[key]["language"]; ok {
			language_string = val
		} else {
			language_string = "Unknown language `" + loc + "`"
		}
		langs[counter] = Language{loc, key, language_string}
		counter++
	}
	return langs
}

func (p *Page) ActiveLanguage() string {
	for loc, key := range *config.Locales {
		if key == p.Locale {
			return loc
		}
	}
	return ""
}

func (p *Page) DefaultLanguage() string {
	return config.DefaultLocale
}
