package templates

import (
	"fmt"
	"strings"
)

type Alternative struct {
	Locale string
	Url    string
}

func (p *Page) Alternatives() []*Alternative {
	if p.alts == nil {
		urlSet := (*config.URLs)[p.Locale]

		urlSplit := strings.Split(p.Url[3:len(p.Url)], "?")
		url := urlSplit[0]
		params_arr := urlSplit[1:]
		params := "?" + strings.Join(params_arr, "?")
		if len(params) == 1 {
			params = ""
		}
		indexResult := 0

		for index, s := range urlSet {
			if s == url {
				indexResult = index
				break
			}
		}

		if indexResult == 0 {
			// Try again by skipping the last parameter
			urlSplit = strings.Split(url, "/")
			url = strings.Join(urlSplit[0:len(urlSplit)-1], "/")
			params = "/" + urlSplit[len(urlSplit)-1] + params
			if len(url) == 0 {
				url = "/"
			}
			for index, s := range urlSet {
				if s == url {
					indexResult = index
					break
				}
			}

			if indexResult == 0 {
				fmt.Println("[Alternatives] Error: index not found for page", url, p.Url)
				return []*Alternative{}
			}

		}

		result := make([]*Alternative, len(*config.URLs))
		counter := 0
		for locale, urlMap := range *config.URLs {
			locString := (*config.Language)[locale]["locale"]
			result[counter] = &Alternative{locString, "/" + locString + urlMap[indexResult] + params}
			counter++
		}
		p.alts = result
	}
	return p.alts
}
