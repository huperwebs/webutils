package templates

import (
	"fmt"
	"log"
	"net/http"
)

// WritePlain writes the given message to the ResponseWriter, with Status code 'status'
func WritePlain(w http.ResponseWriter, status int, msg string) {
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(status)
	w.Write([]byte(msg))
}

// WriteInternalError writes the given error to the ResponseWriter as plaintext,
// and logs the error.
func WriteInternalError(w http.ResponseWriter, err string) {
	msg := fmt.Sprintf("An error has occured: %v", err)
	WritePlain(w, http.StatusInternalServerError, msg)
	log.Println(msg)
}
