package templates

import (
	"fmt"
	"strings"
)

// TODO: make efficient?
func (p *Page) FormatCurrency(f float64) string {
	str := strings.Replace(fmt.Sprintf("%6.2f", f), ".", ",", -1)
	for i := 0; i < len(str)-3; i++ {
		if str[i] == '0' {
			str = str[:i] + " " + str[i+1:]
		} else {
			break
		}
	}
	return str
}
