package helpers

import (
	"log"
	"os"
	"path/filepath"
	"sync"
)

type CacheStorage struct {
	// TODO: replace map[string]map[string]bool with map[string]bool and more advanced Delete func
	dbMu sync.RWMutex
	db   map[string]map[string]bool
	ch   chan *cacheMutation
}

type cacheMutation struct {
	UrlPath    string
	RequestUrl string
	Added      bool
}

func (c *CacheStorage) Exists(urlPath, requestUrl string) bool {
	c.dbMu.RLock()
	defer c.dbMu.RUnlock()
	if reqMap, ok := c.db[urlPath]; ok {
		if val, ok := reqMap[requestUrl]; ok {
			return val
		}
	}
	return false
}

func (c *CacheStorage) Delete(urlPath string) {
	mut := &cacheMutation{urlPath, "", false}
	c.ch <- mut
}

func (c *CacheStorage) Add(urlPath, requestUrl string) {
	mut := &cacheMutation{urlPath, requestUrl, true}
	c.ch <- mut
}

func (c *CacheStorage) handle() {
	for {
		select {
		case mut, ok := <-c.ch:
			if !ok {
				return
			}
			mu := *mut
			c.dbMu.Lock()
			if mu.Added {
				if _, ok := c.db[mu.UrlPath]; !ok {
					c.db[mu.UrlPath] = make(map[string]bool, 5)
				}
				c.db[mu.UrlPath][mu.RequestUrl] = true
			} else {
				for s := range c.db[mu.UrlPath] {
					os.Remove(s)
				}
				delete(c.db, mu.UrlPath)
			}
			c.dbMu.Unlock()
		}
	}
}

func NewCacheStorage(cacheDir string) *CacheStorage {
	cs := &CacheStorage{}
	cs.ch = make(chan *cacheMutation, 10)
	cs.db = make(map[string]map[string]bool, 100)
	log.Println("Clearing cache", filepath.Join(cacheDir, "*"))
	err := os.RemoveAll(filepath.Join(cacheDir))
	if err != nil {
		log.Println("Could not delete cache:", err)
	}
	go cs.handle()
	return cs
}
