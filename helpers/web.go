package helpers

import (
	"github.com/gorilla/context"
	"net/http"
	"net/url"
	"runtime"
	"strconv"
	"strings"
)

// GetExtension goes from "https://aa.bb/abc/def.ghi?jkl=mnp&qrs=tuv" to "ghi"
func GetExtension(filename *string) string {
	without_params := strings.Split(*filename, "?")[0]
	s := strings.Split(without_params, ".")
	return s[len(s)-1]
}

// ExtractFilePath goes from "https://aa.bb/abc/def.ghi?jkl=mnp&qrs=tuv" to ("abc/", "def", "ghi")
func ExtractFilePath(url string) (string, string, string) {
	s := strings.Split(strings.Split(url, "?")[0], "/")
	name := s[len(s)-1]
	splitName := strings.Split(name, ".")
	var ext string
	if len(splitName) == 1 {
		ext = "webp"
		splitName = append(splitName, "")
	} else {
		ext = splitName[len(splitName)-1]
		arr := strings.Split(ext, "?")
		ext = arr[0]
	}
	name = ""
	for _, s := range splitName[0 : len(splitName)-1] {
		name += s + "."
	}
	name = name[0 : len(name)-1]
	path := ""
	for _, s := range s[0 : len(s)-1] {
		path += s + "/"
	}
	return path, name, ext
}

// LastURLParameter treats everything after the last slash (up until the question mark) as parameter-value
func LastURLParameter(r *http.Request) string {
	s := strings.Split(r.URL.Path, "/")
	return s[len(s)-1]
}

// GetStringArray tries to retrieve the context.Get(r, name) value as an []string
func GetStringArray(r *http.Request, name string) []string {
	if listInterface, ok := context.GetOk(r, name); ok {
		if list, ok := listInterface.([]string); ok {
			return list
		}
	}
	return []string{}
}

// ToFriendly converts a string to a HTML-friendly version. Replaces &, /, and space. Trims dashes and slashes.
func ToFriendly(s string) string {
	done := strings.ToLower(s)

	// Replace illegal characters
	type replacer struct {
		Search  string
		Replace string
	}
	replacing := []replacer{
		{" ", "-"},
		{"&", "-"},
		{"`", ""},
		{"'", ""},
		{"\"", ""},
		{"!", ""},
	}
	for _, rep := range replacing {
		done = strings.Replace(done, rep.Search, rep.Replace, -1)
	}

	// Trim bad stuff
	done = strings.Trim(done, "/-")

	// Catch everything we've missed
	return url.QueryEscape(done)
}

func RemoveFromStringArray(arr *[]string, elem string) {
	a := *arr
	for index, s := range a {
		if s == elem {
			RemoveFromStringArrayIndex(arr, index)
			return
		}
	}
}

func RemoveFromStringArrayIndex(arr *[]string, elem int) {
	a := *arr
	a = append(a[:elem], a[elem+1:]...)
	*arr = a
}

func FormInteger(r *http.Request, name string) int {
	str := r.FormValue(name)
	i, err := strconv.Atoi(str)
	if err != nil {
		return 0
	} else {
		return i
	}
}

// MaxParallelism runs the maximum number of goroutines that should be created
func MaxParallelism() int {
	maxProcs := runtime.GOMAXPROCS(0)
	numCPU := runtime.NumCPU()
	if maxProcs < numCPU {
		return maxProcs
	}
	return numCPU
}
