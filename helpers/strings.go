package helpers

func Filter(str string, search string) string {
	for j := 0; j < len(search); j++ {
		for i := 0; i < len(str); i++ {
			if str[i] == search[j] {
				str = str[:i] + str[i+1:]
			}
		}
	}
	return str
}
