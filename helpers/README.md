# HuperWebs

## helpers
This module contains several helper-functions that should make building web applications with Golang easier. 

### GetExtension
GetExtension parses the given string, by taking everything before the optional question-mark (?), then taking everything after the last dot. If there is no extension in the filename, this function will return the entire filename (without parameters). 

### ExtractFilePath
ExtractFilePath extracts the path (directory), name (without extension) and extension. The parameters (after '?') are stripped entirely.
 
### GetStringArray
GetStringArray allows easy parsing of context string-array values. 