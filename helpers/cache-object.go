package helpers

import "time"

type CacheObject struct {
	// Expires is the expiration-interval after which the object should be fetched again
	Expires time.Duration

	// Fetch is the function that retrieves the value from the source
	Fetch func() interface{}

	// date is the date at which this object expires
	date time.Time

	// innerval is the value of the object
	innerval interface{}
}

// Val fetches the actual value from source using the Fetch function - only when needed.
func (o *CacheObject) Val() interface{} {
	if time.Now().After(o.date) {
		o.innerval = o.Fetch()
		o.date = time.Now().Add(o.Expires)
	}
	return o.innerval
}

func NewCacheObject(exp time.Duration, f func() interface{}) CacheObject {
	o := CacheObject{exp, f, time.Now(), nil}
	return o
}
