package openstack

import "github.com/ncw/swift"

type OpenStack struct {
	PublicURL string
	Connected bool

	tempURLKey []byte
	Inner      *swift.Connection
}

func NewOpenStack(publicURL, username, apiKey, tenantId, authURL string, tempURLKey string, production bool) (*OpenStack, error) {
	o := &OpenStack{}
	o.PublicURL = publicURL
	o.tempURLKey = []byte(tempURLKey) // TODO: read from server

	o.Inner = &swift.Connection{
		UserName: username,
		ApiKey:   apiKey,
		TenantId: tenantId,
		AuthUrl:  authURL,
		Internal: production,
	}
	err := o.Inner.Authenticate()
	if err != nil {
		o.Connected = false
		return o, err // some features are available, even though we couldn't connect
	} else {
		o.Connected = true
	}

	return o, nil
}
