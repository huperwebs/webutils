package openstack

import (
	"fmt"
	"io"
	"os"
)

var (
	ERR_NOT_CONNECTED = fmt.Errorf("OpenStack not connected")
)

// Upload tries to upload a file from the local filesystem
func (o *OpenStack) Upload(localPath, remoteContainer, remotePath, contentType string) error {
	if !o.Connected {
		return ERR_NOT_CONNECTED
	}

	file, err := os.Open(localPath)
	if err != nil {
		return err
	}
	return o.UploadReader(file, remoteContainer, remotePath, contentType)
}

// UploadReader uploads the given stream to the remote container/path, with the given Content Type
func (o *OpenStack) UploadReader(file io.Reader, remoteContainer, remotePath, contentType string) error {
	_, err := o.Inner.ObjectPut(remoteContainer, remotePath, file, true, "", contentType, nil)
	if err != nil {
		return err
	}

	return nil
}
