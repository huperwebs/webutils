package openstack

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	// method is the only allowed method; GET
	method = "GET"
)

// sign signs the given bytes with the given key
func sign(value []byte, key []byte) string {
	mac := hmac.New(sha1.New, key)
	n, err := mac.Write(value)
	MAC := mac.Sum(nil)
	if err != nil || n == 0 {
		return ""
	} else {
		return hex.EncodeToString(MAC)
	}
}

// TempAccess provides a URL for the given resource, valid for exp time
func (o *OpenStack) TempAccess(path string, e time.Duration, fileName string) string {
	// Compute end time
	exp := strconv.FormatInt(time.Now().Add(e).Unix(), 10)
	path = strings.Replace(url.QueryEscape(path), "%2F", "/", -1)

	// Sign
	var signable bytes.Buffer
	signable.WriteString(method)
	signable.WriteRune('\n')
	signable.WriteString(exp)
	signable.WriteRune('\n')
	signable.WriteString(path)

	// Compose result
	var buf bytes.Buffer
	buf.WriteString(o.PublicURL)
	buf.WriteString(path)
	buf.WriteString("?temp_url_sig=")
	buf.WriteString(sign(signable.Bytes(), o.tempURLKey))
	buf.WriteString("&temp_url_expires=")
	buf.WriteString(exp)
	if len(fileName) > 0 {
		buf.WriteString("&filename=")
		buf.WriteString(fileName)
	}
	return buf.String()
}
