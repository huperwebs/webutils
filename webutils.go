package webutils

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"strings"

	"crypto/rand"
	"encoding/hex"
	"golang.org/x/net/html"
	"log"
)

func PanicOnError(err error) {
	if err != nil {
		panic(err)
	}
}

func Btoi(b bool) int {
	if b {
		return 1
	} else {
		return 0
	}
}

func Itob(i int) bool {
	return i != 0
}

func ToBytes(i interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(i)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func FromBytes(b []byte, i interface{}) error {
	buf := bytes.NewBuffer(b)
	dec := gob.NewDecoder(buf)
	return dec.Decode(i)
}

func JSArgs(args ...interface{}) map[string]interface{} {
	m := make(map[string]interface{}, len(args)/2)
	for i := 0; i < len(args)-1; i += 2 {
		if s, ok := args[i].(string); ok {
			m[s] = args[i+1]
		} else {
			panic("Uneven arguments should be a string")
		}
	}
	return m
}

func FindNodeWithId(node *html.Node, id string) *html.Node {
	// Try to find it within the children
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		for _, a := range c.Attr {
			if a.Key == "id" && a.Val == id {
				return c
			}
		}
		// Check their children
		rec := FindNodeWithId(c, id)
		if rec != nil {
			return rec
		}
	}
	return nil
}

func FindNodeWithName(node *html.Node, name string) *html.Node {
	// Try to find it within the children
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		for _, a := range c.Attr {
			if a.Key == "name" && a.Val == name {
				return c
			}
		}
		// Check their children
		rec := FindNodeWithName(c, name)
		if rec != nil {
			return rec
		}
	}
	return nil
}

func PrintPrettyHTMLNode(node *html.Node) string {
	switch node.Type {
	case html.TextNode:
		return strings.TrimSpace(node.Data)
	case html.DocumentNode:
		fallthrough
	case html.ElementNode:
		var buf bytes.Buffer
		buf.WriteRune('<')
		buf.WriteString(strings.TrimSpace(node.Data))
		buf.WriteRune('>')
		for c := node.FirstChild; c != nil; c = c.NextSibling {
			buf.WriteString(PrintPrettyHTMLNode(c))
		}
		buf.WriteString("</")
		buf.WriteString(strings.TrimSpace(node.Data))
		buf.WriteRune('>')
		return buf.String()

	default:
		log.Println("UNknown node type:", node.Type)
		return ""
	}
}

func AttributeValue(node *html.Node, key string) string {
	for _, a := range node.Attr {
		if a.Key == key {
			return a.Val
		}
	}
	return ""
}

type HtmlNode struct {
	*html.Node
}

func From(n *html.Node) *HtmlNode {
	return &HtmlNode{n}
}

func (n *HtmlNode) Tag(tag string) (nodes HtmlNodes) {
	if n == nil || n.Node == nil || n.FirstChild == nil {
		return
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		// TODO: what if it has children?
		if strings.TrimSpace(c.Data) == tag {
			nodes = append(nodes, &HtmlNode{c})
		}
	}
	return
}

type HtmlNodes []*HtmlNode

func (nodes HtmlNodes) Tag(tag string) HtmlNodes {
	if len(nodes) == 0 {
		panic(fmt.Errorf("Nodes empty when looking for tag: %s", tag))
	} else if len(nodes) > 1 {
		panic("Nodes must contain exactly one element")
	}
	return nodes[0].Tag(tag)
}

func RandomCrypto(length int) (string, error) {
	b := make([]byte, length)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(b), nil
}
