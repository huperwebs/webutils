// +build cgo

package handlers

import (
	"bitbucket.org/huperwebs/webutils/helpers"

	"github.com/chai2010/webp"
	"github.com/gorilla/context"

	"bytes"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/julienschmidt/httprouter"
)

type WebPConfig struct {
	ContextNameCache string
	ParameterWidth   string
	ParameterHeight  string
	VerboseOutput    bool
	ProjectRoot      string
	ProjectCache     string

	Cache *helpers.CacheStorage
}

func NewWebPHandler(h httprouter.Handle, c *WebPConfig) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		accepts := r.Header.Get("Accept")
		path, name, ext := helpers.ExtractFilePath(r.RequestURI)

		if _, err := os.Stat(filepath.Join(c.ProjectRoot, path, name+"."+ext)); err == nil {
			if IsImageExtension(ext) {
				if strings.Contains(accepts, "image/webp") {
					// Can convert image to WebP
					if !exists(c, r, "webp") {
						create(c, r, "webp")
						if c.VerboseOutput {
							//fmt.Println("WebPHandler: created webp file for", r.RequestURI)
						}
					}

					// Change RequestURI to request the WebP cached version
					if context.Get(r, c.ContextNameCache) == "1" {
						r.RequestURI = strings.Replace(r.RequestURI, "."+ext, ".webp", -1)
					} else {
						r.RequestURI = filepath.Join(c.ProjectCache, path, name+".webp")
						context.Set(r, c.ContextNameCache, "1")
					}
				} else {
					// JPEG
					if ext == "webp" {
						ext = "jpg"
					}
					if ext == "jpeg" || ext == "jpg" {
						w.Header().Set("Content-Type", "image/jpeg")
					} else if ext == "png" {
						w.Header().Set("Content-Type", "image/png")
					} else if ext == "gif" {
						w.Header().Set("Content-Type", "image/gif")
					} else if ext == "webp" {
						w.Header().Set("Content-Type", "image/webp")
					} else {
						fmt.Println("WebP: unknown image format", ext)
					}

					if !exists(c, r, ext) {
						create(c, r, ext)
						if c.VerboseOutput {
							//fmt.Println("WebPHandler: created webp file for", r.RequestURI)
						}
					}

					// Change RequestURI to request the WebP cached version
					if context.Get(r, c.ContextNameCache) == "1" {
						r.RequestURI = filepath.Join(path, name+"."+ext)
					} else {
						r.RequestURI = filepath.Join(c.ProjectCache, path, name+"."+ext)
						context.Set(r, c.ContextNameCache, "1")
					}
				}
			}
		}
		h(w, r, p)
	}
}

func exists(c *WebPConfig, r *http.Request, filetype string) bool {
	path, name, _ := helpers.ExtractFilePath(r.RequestURI)
	var cachePrefix string

	// Find out where the image could be
	if context.Get(r, c.ContextNameCache) == "1" {
		cachePrefix = c.ProjectRoot
	} else {
		cachePrefix = filepath.Join(c.ProjectRoot, c.ProjectCache)
	}

	filename := filepath.Join(cachePrefix, path, name) + "." + filetype
	return c.Cache.Exists(r.URL.Path, filename)
}

func create(c *WebPConfig, r *http.Request, filetype string) {
	var defaultQuality int
	switch filetype {
	case "jpg":
		defaultQuality = 90
	case "webp":
		defaultQuality = 80
	}

	path, name, ext := helpers.ExtractFilePath(r.RequestURI)
	var buf bytes.Buffer

	// open the file
	file, err := os.Open(filepath.Join(c.ProjectRoot, path) + "/" + name + "." + ext)
	if err != nil {
		fmt.Println(err, ext)
	}

	// decode image into image.Image
	img, _, err := image.Decode(file)
	if err != nil {
		fmt.Println(err, ext)
	}

	var quality float32

	if q := helpers.FormInteger(r, "q"); q != 0 {
		quality = float32(q)
	} else {
		quality = float32(defaultQuality)
	}

	// Encode
	switch filetype {
	case "png":
		err = png.Encode(&buf, img)
	case "gif":
		err = gif.Encode(&buf, img, nil)
	case "jpg":
		fallthrough
	case "jpeg":
		err = jpeg.Encode(&buf, img, &jpeg.Options{int(quality)})
	case "webp":
		err = webp.Encode(&buf, img, &webp.Options{false, quality})
	}

	if err != nil {
		fmt.Println(err, ext)
	}

	var prefix string
	if context.Get(r, c.ContextNameCache) == "1" {
		prefix = c.ProjectRoot
	} else {
		prefix = filepath.Join(c.ProjectRoot, c.ProjectCache)
	}
	dir := filepath.Join(prefix, path)
	os.MkdirAll(dir, 0770)

	filename := dir + "/" + name + "." + filetype

	// Write file to disk
	if err = ioutil.WriteFile(filename, buf.Bytes(), 0660); err != nil {
		fmt.Println(err, ext, context.Get(r, c.ContextNameCache), r.RequestURI)
	}

	file.Close()
	c.Cache.Add(r.URL.Path, filename)
}

func IsImageExtension(ext string) bool {
	list := []string{"webp", "jpeg", "jpg", "png", "gif", "bmp", "tiff"}
	for _, l := range list {
		if strings.EqualFold(ext, l) {
			return true
		}
	}
	return false
}
