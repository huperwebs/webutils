package handlers

import (
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strconv"
)

type RetinaConfig struct {
	ContextName string
}

type RetinaHandler struct {
	config *RetinaConfig
}

// NewRetinaHandler creates a new handler that handles internationalization with the given configuration
func NewRetinaHandler(h httprouter.Handle, o *RetinaConfig) httprouter.Handle {
	if len(o.ContextName) == 0 {
		return h
	}

	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		if device, err := r.Cookie("device-pixel-ratio"); err == nil {
			ratio, err := strconv.ParseFloat(device.Value, 32)
			if err == nil {
				context.Set(r, o.ContextName, ratio)
			} else {
				context.Set(r, o.ContextName, 1.0)
			}
		} else {
			context.Set(r, o.ContextName, 1.0)
		}
		h(w, r, p)
	}
}
