package handlers_test

import (
	"bitbucket.org/huperwebs/webutils/handlers"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIsImageExtension(t *testing.T) {
	a := assert.New(t)

	a.Equal(handlers.IsImageExtension("webp"), true, "webp is an image extension")
	a.Equal(handlers.IsImageExtension("WEBP"), true, "WEBP is an image extension")
	a.Equal(handlers.IsImageExtension("jpeg"), true, "jpeg is an image extension")
	a.Equal(handlers.IsImageExtension("JPEG"), true, "JPEG is an image extension")
	a.Equal(handlers.IsImageExtension("jpg"), true, "jpg is an image extension")
	a.Equal(handlers.IsImageExtension("JPG"), true, "JPG is an image extension")
	a.Equal(handlers.IsImageExtension("png"), true, "png is an image extension")
	a.Equal(handlers.IsImageExtension("PNG"), true, "PNG is an image extension")
	a.Equal(handlers.IsImageExtension("gif"), true, "gif is an image extension")
	a.Equal(handlers.IsImageExtension("GIF"), true, "GIF is an image extension")
	a.Equal(handlers.IsImageExtension("bmp"), true, "bmp is an image extension")
	a.Equal(handlers.IsImageExtension("BMP"), true, "BMP is an image extension")
	a.Equal(handlers.IsImageExtension("tiff"), true, "tiff is an image extension")
	a.Equal(handlers.IsImageExtension("TIFF"), true, "TIFF is an image extension")

	a.Equal(handlers.IsImageExtension("doc"), false, "doc is NOT an image extension")
	a.Equal(handlers.IsImageExtension("xml"), false, "xml is NOT an image extension")
	a.Equal(handlers.IsImageExtension("json"), false, "json is NOT an image extension")
	a.Equal(handlers.IsImageExtension("html"), false, "html is NOT an image extension")

	a.Equal(handlers.IsImageExtension(".jpeg"), false, ".jpeg is an image extension, but shouldn't contain the preceiding dot")
}
