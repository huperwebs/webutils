package handlers

import (
	"fmt"
	"net/http"

	"bitbucket.org/huperwebs/webutils/helpers"
	"github.com/julienschmidt/httprouter"
)

// NewMissingExtHandler creates a new handler that uses the default webp-extension for files without one
func NewExpirationHandler(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		ext := helpers.GetExtension(&r.RequestURI)
		switch ext {
		case "jpg":
			fallthrough
		case "png":
			fallthrough
		case "gif":
			fallthrough
		case "webp":
			fallthrough //Mon Jan 2 15:04:05 -0700 MST 2006
		case "js":
			fallthrough
		case "css":
			fallthrough
		case "ico":
			w.Header().Set("Cache-Control", "max-age=604800") //, time.Now().Add(time.Hour * 24 * 7).Format("Mon, 02 Jan 2006 15:04:05 MST"))
		default:
			fmt.Println("No can do", ext)
		case "woff":
			w.Header().Set("Cache-Control", "max-age=31536000")
		}
		h(w, r, p)
	}
}
