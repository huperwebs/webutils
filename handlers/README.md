# HuperWebs
## handlers

### GZipHandler
The GZipHandler performs a gzip-operation on statis resources, saving them in the cache directory. Future requests will use that same cache file, regardless of the original resource (which may have changed). 

### ImageResizeHandler
The ImageResizeHandler checks for requests to JPG-images, and resizes those to the requested max-width/height. The image is saved in the cache directory, and the RequestURI is updated to reflect as such. 

### LanguageHandler
The LanguageHandler saves the locale-id (integer) in the context information for the given Request - for later use in the response process.

### SPDYHandler
Experimental. Not yet usable.

### WebPHandler
The WebPHandler converts images to webp-images when someone requests a jpeg-image and accepts image/webp. The image is saved in the cache directory. Future requests will use that same cache file, regardless of the original resource (which may have changed).

### StaticHandler
An encapsulation of the ```GZipHandler```, ```ImageResizeHandler``` and ```WebPHandler```. 

### DynamicHandler
An encapsulation of the ```http.DefaultServeMux```, ```LanguageHandler``` and ```context.ClearHandler```.