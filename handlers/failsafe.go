package handlers

import (
	"bitbucket.org/huperwebs/webutils/templates"
	"log"
	"net/http"
	"reflect"
)

type FailsafeHandler struct {
	next http.Handler
}

func (f *FailsafeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if rec := recover(); rec != nil {
			err, ok := rec.(error)
			if ok {
				templates.WriteInternalError(w, err.Error())
			} else if errString, ok := rec.(string); ok {
				templates.WriteInternalError(w, errString)
			} else {
				log.Println("An error has occured:", reflect.TypeOf(rec).String())
				templates.WritePlain(w, http.StatusInternalServerError, "Unknown error occured")
			}
		}
	}()
	f.next.ServeHTTP(w, r)
}

func NewFailsafeHandler(h http.Handler) http.Handler {
	return &FailsafeHandler{h}
}
