package handlers

import (
	"net/http"

	"bitbucket.org/huperwebs/webutils/authentication"
	"bitbucket.org/huperwebs/webutils/templates"
	"github.com/gorilla/sessions"
)

type DynamicConfig struct {
	Locales           *map[string]int
	Language          *map[int]map[string]string
	URLs              *map[int]map[int]string
	DefaultLocale     string
	ContextNameLocale string
	ProjectView       string
	Mux               http.Handler
	GetUser           func(r *http.Request) authentication.User
	CookieStore       *sessions.CookieStore
	LoginUrl          string
	ErrorTemplate     string
	ForbiddenURL      string
	Failsafe          bool
}

func NewDynamicHandler(o *DynamicConfig) http.Handler {
	h := o.Mux
	if h == nil {
		h = templates.HttpMux
	}

	authentication.GetUser = authentication.NewGetUser(o.CookieStore)
	authentication.LoginUrl = o.LoginUrl
	authentication.ForbiddenURL = o.ForbiddenURL

	lang := NewLanguageHandler(h, &LanguageConfig{
		Locales:       o.Locales,
		DefaultLocale: o.DefaultLocale,
		ContextName:   o.ContextNameLocale,
	})

	templates.ErrorPage = o.ErrorTemplate
	templates.PreloadTemplate(o.ErrorTemplate)
	templates.PreloadTemplates()

	//gzip := NewGZipDynamicHandler(lang)
	//clear := context.ClearHandler(time)

	if o.Failsafe {
		return NewFailsafeHandler(lang)
	} else {
		return lang
	}
}
