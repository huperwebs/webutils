package handlers

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strings"
)

type MissingExtHandler struct {
	n httprouter.Handle
}

// NewMissingExtHandler creates a new handler that uses the default webp-extension for files without one
func NewMissingExtHandler(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		lastPartArr := strings.Split(r.RequestURI, "/")
		lastPart := lastPartArr[len(lastPartArr)-1]
		lastPartWithoutParamArr := strings.Split(lastPart, "?")
		lastPart = lastPartWithoutParamArr[0]
		if !strings.Contains(lastPart, ".") {
			lastPart += ".webp"
			r.RequestURI = strings.Join(lastPartArr[0:len(lastPartArr)-1], "/") + "/" + lastPart + "?" + strings.Join(lastPartWithoutParamArr[1:], "?")
		}
		h(w, r, p)
	}
}
