// +build cgo

package handlers

import (
	"fmt"
	"image"
	"image/png"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"bitbucket.org/huperwebs/webutils/helpers"
	"github.com/chai2010/webp"
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	"github.com/nfnt/resize"
)

type ResizeConfig struct {
	ContextNameCache string
	ContextNameRatio string
	ParameterHeight  string
	ParameterWidth   string
	VerboseOutput    bool
	ProjectRoot      string
	ProjectCache     string
	Cache            *helpers.CacheStorage
}

func NewImageResizeHandler(h httprouter.Handle, o *ResizeConfig) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		if generateThumbnailIfNeeded(o, r) {
			context.Set(r, o.ContextNameCache, "1")
		}
		h(w, r, p)
	}
}

func getRatio(r *http.Request, name string) float32 {
	if val, ok := context.GetOk(r, name); ok {
		if fl, ok := val.(float64); ok {
			return float32(fl)
		}
	}
	return 1.0
}

// generateThumbnailIfNeeded only creates a thumbnail if (1) it's a JPG-image, and (2) if it's not already created. Alters RequestURI on success.
func generateThumbnailIfNeeded(c *ResizeConfig, r *http.Request) bool {
	fileLocation := filepath.Join(c.ProjectRoot, r.RequestURI)
	fileLocation = strings.Split(fileLocation, "?")[0]
	if _, err := os.Stat(fileLocation); /*helpers.GetExtension(&r.RequestURI) == "webp" && */ err == nil {
		width := r.FormValue(c.ParameterWidth)
		height := r.FormValue(c.ParameterHeight)

		var maxWidth uint
		var maxHeight uint
		quality := helpers.FormInteger(r, "q")
		if width != "" {
			maxWidth64, _ := strconv.ParseUint(width, 10, 32)
			maxWidth = uint(maxWidth64)
		}
		if height != "" {
			maxHeight64, _ := strconv.ParseUint(height, 10, 32)
			maxHeight = uint(maxHeight64)
		}
		if quality == 0 {
			quality = 100
		}

		if ratio := getRatio(r, c.ContextNameRatio); ratio >= 1.0 {
			maxWidth = uint(ratio * float32(maxWidth))
			maxHeight = uint(ratio * float32(maxHeight))
		}

		if maxHeight != 0 || maxWidth != 0 || quality != 100 {
			r.RequestURI = generateThumbnail(c, r.RequestURI, r.URL.Path, maxWidth, maxHeight, quality)
			return true
		}
	}
	return false
}

// generateThumbnail creates a thumbnail for image requested via url, with maxWidth/maxHeight given
func generateThumbnail(c *ResizeConfig, url, urlPath string, maxWidth, maxHeight uint, quality int) string {
	path, name, ext := helpers.ExtractFilePath(url)
	newName := filepath.Join(c.ProjectCache, path) + "/" + name + fmt.Sprintf("_%d_%d_%d", maxWidth, maxHeight, quality) + "." + ext
	fileLoc := filepath.Join(c.ProjectRoot, newName)
	// check if file exists, if so: output it
	if c.Cache.Exists(urlPath, fileLoc) {
		return newName
	} else {
		file, err := os.Open(filepath.Join(c.ProjectRoot, path, name) + "." + ext)
		if err != nil {
			fmt.Println(err)
			// TODO: return with error
		}

		// decode jpeg into image.Image
		var img image.Image
		if ext == "png" { // TODO: I thought we only did JPEG's?
			img, err = png.Decode(file)
		} else {
			img, _, err = image.Decode(file)
		}

		if err != nil {
			fmt.Println("ImageResize line 108: ", err, ext)
		}
		file.Close()

		// resize to width 1000 using Lanczos resampling
		// and preserve aspect ratio
		var m image.Image
		// TODO: make up mind
		//if maxWidth <= 100 || maxHeight <= 100 {
		//		m = resize.Thumbnail(maxWidth, maxHeight, img, resize.NearestNeighbor)
		//	} else if maxWidth <= 300 || maxHeight <= 300 {
		//		m = resize.Thumbnail(maxWidth, maxHeight, img, resize.MitchellNetravali)
		//	} else {
		if maxWidth != 0 && maxHeight != 0 {
			m = resize.Thumbnail(maxWidth, maxHeight, img, resize.Lanczos3)
		} else {
			m = img
		}
		//	}

		os.MkdirAll(filepath.Join(c.ProjectRoot, c.ProjectCache, path), 0770)
		out, err := os.Create(filepath.Join(c.ProjectRoot, newName))
		if err != nil {
			fmt.Println(err)
		}
		defer out.Close()

		// write new image to file
		webp.Encode(out, m, &webp.Options{false, float32(quality)})
		c.Cache.Add(urlPath, fileLoc)

		if c.VerboseOutput {
			//fmt.Println("ImageResizeHandler: created resized image for", url)
			//fmt.Println("   at location: ", newName)
		}
		return newName
	}
}
