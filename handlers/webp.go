// +build !cgo

package handlers

import (
	"bitbucket.org/huperwebs/webutils/helpers"
	"github.com/julienschmidt/httprouter"
)

type WebPConfig struct {
	ContextNameCache string
	ParameterWidth   string
	ParameterHeight  string
	VerboseOutput    bool
	ProjectRoot      string
	ProjectCache     string

	Cache *helpers.CacheStorage
}

func NewWebPHandler(h httprouter.Handle, c *WebPConfig) httprouter.Handle {
	return h
}
