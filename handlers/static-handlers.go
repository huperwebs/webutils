package handlers

import (
	"net/http"
	"path/filepath"

	"bitbucket.org/huperwebs/webutils/helpers"
	"github.com/julienschmidt/httprouter"
)

type StaticConfig struct {
	ContextNameCache string
	ContextNameRatio string
	ParameterHeight  string
	ParameterWidth   string
	VerboseOutput    bool
	PublicRoot       string
	ProjectRoot      string
	ProjectCache     string
	Cache            *helpers.CacheStorage
}

func newHttpRouterHandle(h http.Handler) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		h.ServeHTTP(w, r)
	}
}

func NewStaticHandler(o *StaticConfig) httprouter.Handle {
	publicdir := o.PublicRoot
	if publicdir[0] != '/' {
		publicdir = "/" + publicdir
	}

	localdir := filepath.Join(o.ProjectRoot, o.PublicRoot)
	if localdir[0] != '/' {
		localdir = "./" + localdir
	}

	h := http.StripPrefix(publicdir, http.FileServer(http.Dir(localdir)))

	expireHandler := NewExpirationHandler(newHttpRouterHandle(h))

	gZip := NewGZipHandler(expireHandler, &GZipConfig{
		ContextNameCache: o.ContextNameCache,
		VerboseOutput:    o.VerboseOutput,
		ProjectRoot:      o.ProjectRoot,
		PublicRoot:       o.PublicRoot,
		Cache:            o.Cache,
	})

	webP := NewWebPHandler(gZip, &WebPConfig{
		ContextNameCache: o.ContextNameCache,
		ParameterWidth:   o.ParameterWidth,
		ParameterHeight:  o.ParameterHeight,
		VerboseOutput:    o.VerboseOutput,
		ProjectRoot:      o.ProjectRoot,
		ProjectCache:     o.ProjectCache,
		Cache:            o.Cache,
	})

	resize := NewImageResizeHandler(webP, &ResizeConfig{
		ContextNameCache: o.ContextNameCache,
		ContextNameRatio: o.ContextNameRatio,
		ParameterHeight:  o.ParameterHeight,
		ParameterWidth:   o.ParameterWidth,
		VerboseOutput:    o.VerboseOutput,
		ProjectRoot:      o.ProjectRoot,
		ProjectCache:     o.ProjectCache,
		Cache:            o.Cache,
	})

	retina := NewRetinaHandler(resize, &RetinaConfig{
		ContextName: o.ContextNameRatio,
	})

	missingExt := NewMissingExtHandler(retina)

	return missingExt
}
