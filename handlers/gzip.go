package handlers

import (
	"compress/gzip"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/huperwebs/webutils/helpers"
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
)

type GZipConfig struct {
	ContextNameCache string
	VerboseOutput    bool
	ProjectRoot      string
	PublicRoot       string
	Cache            *helpers.CacheStorage
}

// makeGzip creates a gzipped version of file-location #1 at file-location #2
func makeGzip(filename *string, gzFilename *string) {
	orig, ok1 := ioutil.ReadFile(*filename)
	path := filepath.Dir(*gzFilename)
	os.MkdirAll(path, 0770)
	outfile, ok2 := os.Create((*gzFilename))

	if ok1 != nil {
		msg := fmt.Sprint("OK-1 fails for filename ", *filename, "\n", ok1)
		panic(msg)
	} else if ok2 != nil {
		msg := fmt.Sprint("OK-2 fails for gzFilename ", *gzFilename, "\n", ok2)
		panic(msg)
	}

	gw := gzip.NewWriter(outfile)
	gw.Write(orig)
	gw.Close() // You must close this first to flush the bytes to the buffer.
}

func NewGZipHandler(h httprouter.Handle, o *GZipConfig) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		if strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {

			r.RequestURI = strings.Split(r.RequestURI, "?")[0]

			// The original file is here
			filename := filepath.Join(o.ProjectRoot, r.RequestURI)

			// Make sure we get the URL for the RequestURI in the cache directory
			var reqInCache string
			if val, ok := context.GetOk(r, o.ContextNameCache); ok && val == "1" {
				reqInCache = r.RequestURI
			} else {
				reqInCache = filepath.Join("cache", r.RequestURI)
			}

			// The gzip file should be stored here
			gzFilename := filepath.Join(o.ProjectRoot, reqInCache) + ".gz"

			if !o.Cache.Exists(r.URL.Path, gzFilename) {
				if _, err2 := os.Stat(filename); err2 == nil {
					makeGzip(&filename, &gzFilename)
					if o.VerboseOutput {
						fmt.Println("GZipHandler: created gzip file for", filename)
					}
					o.Cache.Add(r.URL.Path, gzFilename)
				} else {
					w.WriteHeader(http.StatusNotFound)
					if o.VerboseOutput {
						fmt.Println("ERROR 404: Could not find resource \"" + filename + "\"")
					}
					return
				}
			}

			// After making sure the file exists and we're sending the gzip version, set the content-type
			if len(w.Header().Get("Content-Type")) == 0 {
				switch helpers.GetExtension(&filename) {
				case "css":
					w.Header().Set("Content-Type", "text/css")
				case "js":
					w.Header().Set("Content-Type", "text/javascript")
				case "jpg":
					w.Header().Set("Content-Type", "image/jpeg")
				case "png":
					w.Header().Set("Content-Type", "image/png")
				case "gif":
					w.Header().Set("Content-Type", "image/gif")
				case "webp":
					w.Header().Set("Content-Type", "image/webp")
				case "ico":
					w.Header().Set("Content-Type", "image/x-icon")
				case "pdf":
					w.Header().Set("Content-Type", "application/pdf")
				case "map":
					w.Header().Set("Content-Type", "application/json")
				case "woff":
					w.Header().Set("Access-Control-Allow-Origin", "*")
					w.Header().Set("Content-Type", "application/font-woff")
				case "eot":
					w.Header().Set("Access-Control-Allow-Origin", "*")
					w.Header().Set("Content-Type", "application/vnd.ms-fontobject")
				case "ttf":
					w.Header().Set("Access-Control-Allow-Origin", "*")
					w.Header().Set("Content-Type", "application/x-font-ttf")
				case "svg":
					w.Header().Set("Access-Control-Allow-Origin", "*")
					w.Header().Set("Content-Type", "image/svg+xml")
				default:
					fmt.Println("Unsupported type:", filename)
					h(w, r, p)
					return
				}
			}

			// Make sure we're sending the gzipped version and set content-encoding
			r.URL.Path = "/" + filepath.Join(o.PublicRoot, reqInCache+".gz")
			fmt.Println(r.URL.Path)
			w.Header().Set("Content-Encoding", "gzip")
		}

		h(w, r, p)
	}
}
