// +build !cgo

package handlers

import (
	"bitbucket.org/huperwebs/webutils/helpers"
	"github.com/julienschmidt/httprouter"
)

type ResizeConfig struct {
	ContextNameCache string
	ContextNameRatio string
	ParameterHeight  string
	ParameterWidth   string
	VerboseOutput    bool
	ProjectRoot      string
	ProjectCache     string
	Cache            *helpers.CacheStorage
}

func NewImageResizeHandler(h httprouter.Handle, o *ResizeConfig) httprouter.Handle {
	return h
}
