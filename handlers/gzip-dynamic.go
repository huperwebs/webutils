package handlers

import (
	"compress/gzip"
	"io"
	"net/http"
	"strings"
)

type gzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

func (w gzipResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

type GZipDynamicHandler struct {
	next http.Handler
}

func (h *GZipDynamicHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if strings.Index(r.RequestURI, "/public/") != 0 &&
		strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
		w.Header().Set("Content-Encoding", "gzip")

		gw := gzip.NewWriter(w)
		gwr := gzipResponseWriter{Writer: gw, ResponseWriter: w}
		defer gw.Close()
		h.next.ServeHTTP(gwr, r)
	} else {
		h.next.ServeHTTP(w, r)
	}
}

func NewGZipDynamicHandler(h http.Handler) http.Handler {
	return &GZipDynamicHandler{h}
}
