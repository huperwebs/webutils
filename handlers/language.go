package handlers

import (
	"github.com/gorilla/context"

	"log"
	"net/http"
	"strings"

	"bitbucket.org/huperwebs/webutils/templates"
	"github.com/julienschmidt/httprouter"
)

type LanguageConfig struct {
	Locales       *map[string]int
	DefaultLocale string
	ContextName   string
}

type LanguageHandler struct {
	config *LanguageConfig
	next   http.Handler
}

func (h *LanguageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	split := strings.Split(r.RequestURI, "/")
	if len(split) >= 2 {

		// Check if the value between the first two slashes is a valid locale
		if locale, ok := (*h.config.Locales)[split[1]]; ok {
			context.Set(r, h.config.ContextName, locale)
		} else {
			context.Set(r, h.config.ContextName, (*h.config.Locales)[h.config.DefaultLocale])
		}
	} else {
		log.Println("Redirecting to", "/"+h.config.DefaultLocale+"/")
		http.Redirect(w, r, "/"+h.config.DefaultLocale+"/", 301)
		return
	}
	h.next.ServeHTTP(w, r)
}

var mainHandler = func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	log.Println("Default redir")
	http.Redirect(w, r, templates.GetUrl(r, 1), 301)
}

// NewLanguageHandler creates a new handler that handles internationalization with the given configuration
func NewLanguageHandler(h http.Handler, o *LanguageConfig) http.Handler {
	if o.Locales == nil {
		return h
	}

	if o.Locales == nil || o.DefaultLocale == "" {
		panic("Configuration invalid - missing fields. ")
	}
	if o.ContextName == "" {
		o.ContextName = "locale"
	}

	handler := &LanguageHandler{o, h}
	templates.HttpMux.GET("/", mainHandler)
	return handler
}
