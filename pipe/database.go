package pipe

import (
	"encoding/gob"

	"github.com/boltdb/bolt"
)

func Encode(err error, enc *gob.Encoder, i interface{}) error {
	if err == nil {
		err = enc.Encode(i)
	}
	return err
}

func Decode(err error, dec *gob.Decoder, i interface{}) error {
	if err == nil {
		err = dec.Decode(i)
	}
	return err
}

func CreateBucket(err error, tx *bolt.Tx, b []byte) error {
	if err == nil {
		_, err = tx.CreateBucketIfNotExists(b)
	}
	return err
}
