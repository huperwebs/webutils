# WebUtils

This project is a work-in-progress. If there's something that can be re-used in web servers, it can go here. 

A lot of refactoring is yet to be done, so use at own risk. No guarantees whatsoever, a lot may change.  

## Contribute?
Create an issue if you want to discuss. Pull Requests are always welcome. 

## License
MIT