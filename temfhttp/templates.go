package temfhttp

import (
	"bytes"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/HuperWebs/go-html-combiner/combiner"
	"github.com/buaazp/fasthttprouter"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/html"
	"github.com/valyala/fasthttp"
)

var (
	// Directory is the folder in which the template files are expected to be
	Directory = "templates"

	// Minify indicates whether or not the resulting HTML pages should be minified by using
	Minify   = false
	minifier *minify.M

	// NoCache indicates whether or not we should add the Cache-Control header "no-cache"
	NoCache = true

	tmplNames []string
	tmpl      = make(map[string]*template.Template)
	tmplMu    sync.RWMutex
)

func Render(c *fasthttp.RequestCtx, tmpl string, p map[string]interface{}) {
	RenderWithStatus(c, tmpl, p, fasthttp.StatusOK)
}

func RenderWithStatus(c *fasthttp.RequestCtx, templateName string, p map[string]interface{}, status int) {
	if NoCache {
		c.Response.Header.Set("Cache-Control", "no-cache")
	}

	c.SetContentType("text/html")
	c.SetStatusCode(status)

	tmplMu.RLock()
	t, ok := tmpl[filepath.Join(Directory, templateName)]
	tmplMu.RUnlock()

	if !ok {
		log.Println("Could not find template:", filepath.Join(Directory, templateName))
		return
	}

	if p == nil {
		p = map[string]interface{}{}
	}

	for _, h := range handlers {
		p = h(c, p)
	}

	var buffer bytes.Buffer
	if Minify {
		err := t.Execute(&buffer, p)
		if err != nil {
			log.Printf("Warning: unable to minify template %q: %s\n", templateName, err)

			err := t.Execute(c.Response.BodyWriter(), p)
			if err != nil {
				log.Println("Unable to render template after failing to minify:", err)
			}

			return
		}

		// TODO: verify if c.Request.BodyWriter is valid
		err = minifier.Minify("text/html", c.Request.BodyWriter(), &buffer)
		if err != nil {
			log.Println(err)
		}
	} else {
		err := t.Execute(c.Response.BodyWriter(), p)
		if err != nil {
			log.Println(err)
		}
	}
}

// Register adds the given `templateName` to the list of files that need to be loaded on `Preload`
//
// It is by no means thread-safe.
func Register(templateName string) {
	tmplNames = append(tmplNames, templateName)
}

// Preload loads all the previously registered templates (see `Register`) into memory, to be used by `Render`.
func Preload() {
	errDir := filepath.Join(Directory, ".debug")
	os.RemoveAll(errDir)
	os.Mkdir(errDir, os.FileMode(0755))

	for _, t := range tmplNames {
		tpl, err := PreloadTemplateFile(t)
		if err != nil {
			name := filepath.Join(errDir, t)
			os.MkdirAll(filepath.Dir(name), os.FileMode(0755))
			ioutil.WriteFile(name, []byte(tpl), os.FileMode(0755))
			log.Println("Warning: preloading template has failed:", err)
		}
	}
}

// PreloadInMemory loads the internal representation of the different templates, but does not 'register' it with the
// Go templating engine
func PreloadInMemory() {
	for _, t := range tmplNames {
		f, err := os.Open(filepath.Join(Directory, t))
		if err != nil {
			log.Println("Not able to load template", t, err)
			continue
		}
		defer f.Close() // TODO: verify when we need to .Close it

		err = PreloadTemplateInMemory(filepath.Join(Directory, t), f, nil)
		if err != nil {
			log.Println("Unable to load template", t, err)
			continue
		}
	}
}

func PreloadTemplateInMemory(name string, r io.Reader, o *combiner.Options) (error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}

	child, err := combiner.GetSections(string(data), o)
	if err != nil {
		return err
	}

	if len(child) > 0 && child[0].CommandName == combiner.COMMAND_EXTENDS {
		master, err := combiner.LoadExtends(filepath.Join(Directory, child[0].Name))
		if err != nil {
			return err
		}

		err = combiner.CombineSections(Directory, child, master)
		if err != nil {
			return err
		}

		memory[name] = master
	} else {
		master, ok := memory[name]
		if ok {
			// Exists, so we have to plugin-extend it
			err = combiner.CombineSections(Directory, child, master)
			if err != nil {
				return err
			}

			memory[name] = master
		} else {
			// Not exists, so we just save what we have
			memory[name] = child
		}
	}

	return nil
}

var memory map[string]combiner.Template

// PreloadTemplateFile updates the definition of a single template file into memory.
func PreloadTemplateFile(templateName string) (string, error) {
	f, err := os.Open(filepath.Join(Directory, templateName))
	if err != nil {
		return "", err
	}

	tpl, err := PreloadTemplate(filepath.Join(Directory, templateName), f)
	f.Close() // TODO: verify when we need to .Close it

	return tpl, err
}

func PreloadTemplate(name string, r io.Reader) (string, error) {
	c, err := combiner.CombineReader(Directory, r)
	if err != nil {
		return "", err
	}
	s := string(c)

	// Save it to the list

	var t *template.Template
	t = template.New(name)

	tmplMu.Lock()
	tmpl[name] = t
	tmplMu.Unlock()

	_, err = t.Parse(s)
	if err != nil {
		return s, err
	}

	// TODO: figure out why we need/use this one
	t.ParseFiles()
	//if err != nil {
	//	return err
	//}

	return s, nil
}

var handlers []FasthttpHandler

type FasthttpHandler func(ctx *fasthttp.RequestCtx, p map[string]interface{}) map[string]interface{}

func RegisterHandler(h FasthttpHandler) {
	handlers = append(handlers, h)
}

// RegisterPlugin lists the `Plugin` for preloading
func RegisterPlugin(p Plugin) {
	Plugins = append(Plugins, p)
}

// PreloadPlugins 'extends' / 'overwrites' any existing plugins with those defined in the plugins
func PreloadPlugins() {
	for _, p := range Plugins {
		info, err := os.Stat(p.TemplateDir())
		if err != nil {
			log.Println("Unable to get directory info on", p.TemplateDir(), "for", p.Name())
			continue
		}

		if !info.IsDir() {
			log.Println(p.TemplateDir(), "is not a directory; plugin", p.Name())
			continue
		}

		err = filepath.Walk(p.TemplateDir(), func(path string, info os.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}

			return preloadPlugin(path, p)
		})
		if err != nil {
			log.Println("Unable to parse templates for", p.Name(), err)
			continue
		}
	}
}

func preloadPlugin(filename string, p Plugin) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}

	name := filepath.Join(Directory, filepath.Clean(filename[len(p.TemplateDir())+1:]))

	return PreloadTemplateInMemory(name, f, &combiner.Options{
		Prefix: "{{ if (index $p \"" + p.Name() + "\").Enabled }}",
		Suffix: "{{ end }}",
	})
}

// Save is used to push in-memory templates to the Go template engine
func Save() {
	errDir := filepath.Join(Directory, ".debug")
	os.RemoveAll(errDir)
	os.Mkdir(errDir, os.FileMode(0755))

	for k, v := range memory {
		var t *template.Template
		t = template.New(k)

		tmplMu.Lock()
		tmpl[k] = t
		tmplMu.Unlock()

		_, err := t.Parse(v.String())

		if err != nil {
			name := filepath.Join(errDir, k)
			os.MkdirAll(filepath.Dir(name), os.FileMode(0755))
			ioutil.WriteFile(name, []byte(v.String()), os.FileMode(0755))
			log.Println("Warning: preloading template has failed:", strings.Replace(err.Error(), k, name, -1))
			continue
		}

		// TODO: figure out why we need/use this one
		t.ParseFiles()
	}
}

type Plugin interface {
	Name() string
	Enabled() bool
	TemplateDir() string
}

var Plugins []Plugin

var Router = fasthttprouter.New()

func init() {
	memory = make(map[string]combiner.Template)

	minifier = minify.New()
	minifier.AddFunc("text/html", html.Minify)
}
