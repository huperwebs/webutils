package templiris

import (
	"bytes"
	"html/template"
	"io"
	"log"
	"os"
	"path/filepath"
	"sync"

	"github.com/HuperWebs/go-html-combiner/combiner"
	"github.com/kataras/iris"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/html"
)

var (
	// Directory is the folder in which the template files are expected to be
	Directory = "templates"

	// Minify indicates whether or not the resulting HTML pages should be minified by using
	Minify   = false
	minifier *minify.M

	// NoCache indicates whether or not we should add the Cache-Control header "no-cache"
	NoCache = true

	tmplNames []string
	tmpl      = make(map[string]*template.Template)
	tmplMu    sync.RWMutex
)

func Render(c *iris.Context, tmpl string, p map[string]interface{}) {
	RenderWithStatus(c, tmpl, p, iris.StatusOK)
}

func RenderWithStatus(c *iris.Context, templateName string, p map[string]interface{}, status int) {
	if NoCache {
		c.SetHeader("Cache-Control", "no-cache")
	}

	c.SetHeader("Content-Type", "text/html")
	c.SetStatusCode(status)

	tmplMu.RLock()
	t, ok := tmpl[filepath.Join(Directory, templateName)]
	tmplMu.RUnlock()

	if !ok {
		log.Println("Could not find template:", filepath.Join(Directory, templateName))
		return
	}

	if p == nil {
		p = map[string]interface{}{}
	}

	var buffer bytes.Buffer
	if Minify {
		err := t.Execute(&buffer, p)
		if err != nil {
			log.Printf("Warning: unable to minify template %q: %s\n", templateName, err)

			err := t.Execute(c.Response.BodyWriter(), p)
			if err != nil {
				log.Println("Unable to render template after failing to minify:", err)
			}

			return
		}

		// TODO: verify if c.Request.BodyWriter is valid
		err = minifier.Minify("text/html", c.Request.BodyWriter(), &buffer)
		if err != nil {
			log.Println(err)
		}
	} else {
		err := t.Execute(c.Response.BodyWriter(), p)
		if err != nil {
			log.Println(err)
		}
	}
}

// Register adds the given `templateName` to the list of files that need to be loaded on `Preload`
//
// It is by no means thread-safe.
func Register(templateName string) {
	tmplNames = append(tmplNames, templateName)
}

// Preload loads all the previously registered templates (see `Register`) into memory, to be used by `Render`.
func Preload() {
	for _, t := range tmplNames {
		err := PreloadTemplateFile(t)
		if err != nil {
			log.Println("Warning: preloading template", t, "has failed:", err)
		}
	}
}

// PreloadTemplateFile updates the definition of a single template file into memory.
func PreloadTemplateFile(templateName string) error {
	f, err := os.Open(filepath.Join(Directory, templateName))
	if err != nil {
		return err
	}

	err = PreloadTemplate(filepath.Join(Directory, templateName), f)
	f.Close() // TODO: verify when we need to .Close it

	return err
}

// PreloadTemplate updates the definition of a single template from any stream, into memory.
func PreloadTemplate(name string, r io.Reader) error {
	c, err := combiner.CombineReader(Directory, r)
	if err != nil {
		return err
	}
	s := string(c)

	// Save it to the list

	var t *template.Template
	t = template.New(name)

	tmplMu.Lock()
	tmpl[name] = t
	tmplMu.Unlock()

	_, err = t.Parse(s)
	if err != nil {
		return err
	}

	// TODO: figure out why we need/use this one
	t.ParseFiles()
	//if err != nil {
	//	return err
	//}

	return nil
}

func init() {
	minifier = minify.New()
	minifier.AddFunc("text/html", html.Minify)
}
