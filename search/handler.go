package search

import (
	"math"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"sync"

	"github.com/julienschmidt/httprouter"
	"github.com/unrolled/render"
)

type SearchItem struct {
	Searchable string
	Text       string
	URL        string
	Priority   int
	IsProduct  bool
}

func SearchHandleAPIGET(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	q := r.FormValue("q")
	n_str := r.FormValue("n")
	n, err := strconv.Atoi(n_str)
	if err != nil {
		n = 10 // default
	}

	items, terms := db.Search(q, n)
	sort.Stable(items)

	if n > len(items) {
		n = len(items)
	}

	rows := make([]interface{}, len(items[:n]))
	for i, item := range items[:n] {
		row := make([]string, 2)
		row[0] = item.URL
		row[1] = item.Text
		rows[i] = row
	}

	if len(rows) > 1 {
		rows = append([]interface{}{[]string{
			"/zoek/resultaten/?q=" + url.QueryEscape(q),
			"<span class=\"media\">&#xE820;</span> Alle zoekresultaten...",
		}}, rows...)
	}
	if len(rows) == 0 {
		rows = append([]interface{}{[]string{
			"/zoek/resultaten/?q=" + url.QueryEscape(q),
			"<span class=\"media\">&#xE821;</span> Geen zoekresultaten gevonden",
		}}, rows...)
	}

	output := make(map[string]interface{})
	output["terms"] = terms
	output["results"] = rows

	renderer.JSON(w, http.StatusOK, output)
}

type searchDb struct {
	db    map[string][]int
	items []*SearchItem
	mu    sync.RWMutex
}

type results []*SearchItem

func (r results) Len() int {
	return len(r)
}
func (r results) Less(i, j int) bool {
	if r[i].Priority == r[j].Priority {
		return r[i].Text < r[j].Text
	}
	return r[i].Priority > r[j].Priority
}
func (r results) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

func newSearchDb() *searchDb {
	s := &searchDb{}
	s.db = make(map[string][]int)
	return s
}

// Search searches the searchDb for the given string, returning at most limit results
func (s *searchDb) Search(str string, limit int) (results, []string) {
	terms := strings.Split(strings.ToLower(str), " ")
	found := make(map[int]int)
	if limit == 0 {
		limit = math.MaxInt32
	}

	for term_index, term := range terms {
		if arr, ok := s.db[term]; ok {
			for _, index := range arr {
				if term_index == 0 {
					found[index] = 1
				} else {
					if val, ok := found[index]; ok {
						if val == index {
							found[index] = index + 1
						}
					}
				}
			}
		}
	}

	var results = make([]*SearchItem, 0, len(found))
	for k, v := range found {
		if v == len(terms) {
			results = append(results, s.items[k])
		}
	}

	return results, terms
}

// Add adds another searchable SearchItem to the database
func (s *searchDb) Add(si *SearchItem) {
	s.mu.Lock()
	defer s.mu.Unlock()

	index := len(s.items)
	s.items = append(s.items, si)

	split := strings.Split(strings.ToLower(si.Searchable), " ")
	for _, term := range split {
		for i := 1; i <= len(term); i++ {
			t := term[:i]
			var list []int
			if val, ok := s.db[t]; ok {
				list = append(val, index)
			} else {
				list = []int{index}
			}
			s.db[t] = list
		}
	}
}

var db searchDb
var renderer = render.New()

func init() {
	db = *newSearchDb()
}
