package authentication

import (
	"net/http"

	"bitbucket.org/huperwebs/webutils/templates"
	"github.com/julienschmidt/httprouter"
)

type Permission uint8

const (
	// GuestPermission will be given to every single user by default
	GuestPermission Permission = 0
)

var (
	LoginUrl     string
	ForbiddenURL string

	ForbiddenHandler http.HandlerFunc
)

type Auth struct {
	Perm Permission
}

func (a *Auth) HandleFunc(method, path string, h httprouter.Handle) {
	templates.HttpMux.Handle(method, path, a.newVerifyHandler(h))
}

func (a *Auth) GET(path string, h func(http.ResponseWriter, *http.Request, httprouter.Params)) {
	templates.HttpMux.GET(path, a.newVerifyHandler(httprouter.Handle(h)))
}

func (a *Auth) POST(path string, h func(http.ResponseWriter, *http.Request, httprouter.Params)) {
	templates.HttpMux.POST(path, a.newVerifyHandler(httprouter.Handle(h)))
}

func (a *Auth) newVerifyHandler(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		if u := GetUser(r); a.IsAuthenticated(u) {
			h(w, r, p)
		} else if u == GuestUser {
			http.Redirect(w, r, LoginUrl+"?returnUrl="+r.RequestURI, http.StatusFound)
		} else {
			if ForbiddenHandler != nil {
				ForbiddenHandler(w, r)
			} else if len(ForbiddenURL) > 0 {
				http.Redirect(w, r, ForbiddenURL, http.StatusFound)
			} else {
				templates.WritePlain(w, http.StatusForbidden, "Access denied.")
			}
		}
	}
}

func (a *Auth) IsAuthenticated(u User) bool {
	val, ok := u.Permissions()[a.Perm]
	return ok && val
}
