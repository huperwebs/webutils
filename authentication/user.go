package authentication

import (
	"bytes"
	"encoding/gob"
	"net/http"

	"github.com/gorilla/sessions"
)

const (
	sessionName string = "huperwebs-auth"
	sessionKey  string = "authentication"
)

var (
	// GetUser will be called to collect the user data
	GetUser func(r *http.Request) User
)

type User interface {
	Permissions() map[Permission]bool
	Allow(Permission)
	Disallow(Permission)
}

// Creation of users

type BasicUser struct {
	perms map[Permission]bool
}

func (u *BasicUser) Permissions() map[Permission]bool {
	return u.perms
}

func (u *BasicUser) Allow(p Permission) {
	u.perms[p] = true
}

func (u *BasicUser) Disallow(p Permission) {
	delete(u.perms, p)
}

func (u *BasicUser) GobEncode() ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)

	for k, v := range u.Permissions() {
		if err := enc.Encode(k); err != nil {
			return nil, err
		}
		if err := enc.Encode(v); err != nil {
			return nil, err
		}
	}
	return buf.Bytes(), nil
}

func (u *BasicUser) GobDecode(b []byte) error {
	u.perms = make(map[Permission]bool)

	buf := bytes.NewBuffer(b)
	dec := gob.NewDecoder(buf)
	var err error
	for err == nil {
		var k Permission
		var v bool
		err = dec.Decode(&k)
		if err != nil {
			break // because we already did the last one
		}

		err = dec.Decode(&v)
		if err != nil {
			return err // we shouldnt have a value left
		}
		u.perms[k] = v
	}
	return nil
}

var GuestUser = NewUser()

func NewUser() User {
	u := &BasicUser{perms: make(map[Permission]bool)}
	u.Allow(GuestPermission)
	return u
}

// NewGetUser allows you to generate a function which will retrieve user information from
// the given session.CookieStore
func NewGetUser(cs *sessions.CookieStore) func(r *http.Request) User {
	return func(r *http.Request) User {
		if cs == nil {
			return GuestUser
		}
		if s, err := cs.Get(r, sessionName); err == nil {
			if val, ok := s.Values[sessionKey]; ok {
				if u, ok := val.(User); ok {
					return u
				}
			}
		}
		return GuestUser
	}
}

func SaveUser(cs *sessions.CookieStore, r *http.Request, w http.ResponseWriter, u User) {
	s, _ := cs.Get(r, sessionName)
	s.Values[sessionKey] = &BasicUser{u.Permissions()}
	s.Save(r, w)
}
func ClearUser(cs *sessions.CookieStore, r *http.Request, w http.ResponseWriter) {
	s, _ := cs.Get(r, sessionName)
	delete(s.Values, sessionKey)
	s.Save(r, w)
}

func init() {
	gob.Register(&BasicUser{})
}
